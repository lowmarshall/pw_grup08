<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 03/04/2017
 * Time: 16:25
 */

$app->get('/test', 'SilexApp\\Controller\\HomeController::test');

$app->get('/', 'SilexApp\\Controller\\HomeController::loadHome');
$app->get('/{id}/like', 'SilexApp\\Controller\\HomeController::likedPost');
$app->post('/', 'SilexApp\\Controller\\HomeController::commentedPost');

//User links
$app->get('/user/register', 'SilexApp\\Controller\\RegisterController::showForm');
$app->post('/user/register', 'SilexApp\\Controller\\RegisterController::getForm');
$app->get('/user/login', 'SilexApp\\Controller\\LoginController::showForm');
$app->post('/user/login', 'SilexApp\\Controller\\LoginController::getForm');
$app->get('/user/logout', 'SilexApp\\Controller\\LogoutController::closeSession');
$app->get('/user/activate/{nom}', 'SilexApp\\Controller\\ActivationController::activateAccount');
$app->get('/user/profile/private/{nom}', 'SilexApp\\Controller\\PrivateProfileController::showForm');
$app->post('/user/profile/private/{nom}', 'SilexApp\\Controller\\PrivateProfileController::getForm');
$app->get('/user/profile/public/{nom}', 'SilexApp\\Controller\\PublicProfileController::showPage');
$app->get('/user/logout','SilexApp\\Controller\\LogOutController::logout');


$app->get('/user/comments','SilexApp\\Controller\\CommentsController::showList');
$app->get('/user/comments/delete/{id}','SilexApp\\Controller\\CommentsController::deleteComment');

//Image links
$app->get('/image/{id}', 'SilexApp\\Controller\\ImageController::loadImage');
$app->get('/image/{id}/like', 'SilexApp\\Controller\\ImageController::likeImage');
$app->post('/image/{id}', 'SilexApp\\Controller\\ImageController::commentImage');

$app->get('/image/edit/{id}', 'SilexApp\\Controller\\ImageEditController::showForm');
$app->post('/image/edit/{id}', 'SilexApp\\Controller\\ImageEditController::getForm');

$app->get('/user/image-list', 'SilexApp\\Controller\\ImageListController::showList');
$app->get('/user/image-list/delete/{id}', 'SilexApp\\Controller\\ImageListController::deleteImage');
$app->get('/user/upload', 'SilexApp\\Controller\\UploadController::showForm');
$app->post('/user/upload', 'SilexApp\\Controller\\UploadController::uploadImage');




$app->get('/user/notifications', 'SilexApp\\Controller\\NotificationController::showNotifications');