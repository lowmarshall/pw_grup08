<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 30/08/2017
 * Time: 16:13
 */

namespace SilexApp\Controller;


use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

class ImageListController
{
    public function showList(Application $app){


        $response = new Response();




        if ($app['session']->has('user')){

            $sql = "SELECT * FROM post WHERE user_id = ?";
            $posts = $app['db']->fetchAll($sql, array($app['session']->get('user')['id']));

            $content = $app['twig']->render('imagelist.twig', array(
                'online_nav' => true,
                'session_username' => $app['session']->get('user')['username'],
                'images' => $posts
            ));
            $response->setContent($content);
            return $response;

        }else{
            $content = $app['twig']->render('error.twig', array(
                'online_nav' => false,
                'message' => "403: Access denegat"
            ));
            $response->setContent($content);
            return $response;
        }
    }
    public function deleteImage(Application $app, $id){
        $response = new Response();
        if ($app['session']->has('user')){
            $sql = "SELECT * FROM post WHERE id = ?";
            $post = $app['db']->fetchAssoc($sql, array($id));
            if($post){
                if($app['session']->get('user')['id']== $post['user_id']){
                    //elimina la imatge
                    $app['db']->delete('post', array(
                       'id' => $id
                    ));
                    $app['db']->delete('liked_image', array(
                        'post_id' => $id
                    ));
                    $app['db']->delete('comment', array(
                        'post_id' => $id
                    ));
                    return $app->redirect('/user/image-list');
                }else{
                    $content = $app['twig']->render('error.twig', array(
                        'online_nav' => true,
                        'session_username' => $app['session']->get('user')['username'],
                        'message' => "403: Acces denegat"
                    ));

                    $response->setContent($content);
                    return $response;
                }
            }else{
                $content = $app['twig']->render('error.twig', array(
                    'online_nav' => true,
                    'session_username' => $app['session']->get('user')['username'],
                    'message' => "404: La imatge no existeix"
                ));

                $response->setContent($content);
                return $response;
            }
        }else{
            $content = $app['twig']->render('error.twig', array(
                'online_nav' => false,
                'message' => "403: Acces denegat"
            ));

            $response->setContent($content);
            return $response;
        }


    }
}