<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 29/08/2017
 * Time: 22:49
 */

namespace SilexApp\Controller;


use Silex\Application;

class LogoutController
{
    public function closeSession(Application $app){
        $app['session']->clear();
        return $app->redirect('/');
    }
}