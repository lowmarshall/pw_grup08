<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 29/08/2017
 * Time: 1:37
 */

namespace SilexApp\Controller;


use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HomeController
{

    public function loadHome(Application $app){
        $response = new Response();
        if ($app['session']->has('user')){
            $online_nav = true;
            $session_username = $app['session']->get('user')['username'];

            //Carrega dades amb totes les imatges i comentaris

            $sql = "SELECT * FROM post WHERE private = 0 ORDER BY visits DESC LIMIT 5";
            $top_visited = $app['db']->fetchAll($sql);
            $top_visited_real = array();
            foreach($top_visited as $item){

                $sql = "SELECT * FROM user WHERE id = ?";
                $user = $app['db']->fetchAssoc($sql, array($item['user_id']));
                $item['username'] = $user['username'];
                $sql = "SELECT * FROM comment WHERE post_id = ?";
                $comments = $app['db']->fetchAll($sql, array($item['id']));
                $sql = "SELECT * FROM comment WHERE user_id = ? AND post_id = ?";
                $already_commented = $app['db']->fetchAssoc($sql, array($app['session']->get('user')['id'], $item['id']));
                if ($already_commented){
                    $ac = true;
                }else{
                    $ac = false;
                }
                $sql = "SELECT * FROM liked_image WHERE user_id = ? AND post_id = ?";
                $already_liked = $app['db']->fetchAssoc($sql, array($app['session']->get('user')['id'], $item['id']));
                if ($already_liked){
                    $al = true;
                }else{
                    $al = false;
                }
                $sql = "SELECT * FROM liked_image WHERE post_id = ?";
                $likes_array = $app['db']->fetchAll($sql, array($item['id']));
                $likes = count($likes_array);
                $comments_real = array();
                if ($comments){
                    foreach ($comments as $comment){
                        $sql = "SELECT * FROM user WHERE id = ?";
                        $user = $app['db']->fetchAssoc($sql, array($comment['user_id']));
                        $comment['commenter'] = $user['username'];
                        array_push($comments_real, $comment);
                    }
                }
                $item['already_liked'] = $al;
                $item['already_commented'] = $ac;
                $item['likes'] = $likes;
                $item['comments'] = $comments_real;

                array_push($top_visited_real, $item);

            }





            $sql = "SELECT * FROM post WHERE private = 0 ORDER BY created_at DESC LIMIT 5";
            $last_uploaded = $app['db']->fetchAll($sql);
            $last_uploaded_real = array();
            foreach($last_uploaded as $item){
                //recoger información de comentarios, i si es comentable y likeable o no
                $sql = "SELECT * FROM user WHERE id = ?";
                $user = $app['db']->fetchAssoc($sql, array($item['user_id']));
                $item['username'] = $user['username'];
                $sql = "SELECT * FROM comment WHERE post_id = ?";
                $comments = $app['db']->fetchAll($sql, array($item['id']));
                $sql = "SELECT * FROM comment WHERE user_id = ? AND post_id = ?";
                $already_commented = $app['db']->fetchAssoc($sql, array($app['session']->get('user')['id'], $item['id']));
                if ($already_commented){
                    $ac = true;
                }else{
                    $ac = false;
                }
                $sql = "SELECT * FROM liked_image WHERE user_id = ? AND post_id = ?";
                $already_liked = $app['db']->fetchAssoc($sql, array($app['session']->get('user')['id'], $item['id']));
                if ($already_liked){
                    $al = true;
                }else{
                    $al = false;
                }
                $sql = "SELECT * FROM liked_image WHERE post_id = ?";
                $likes_array = $app['db']->fetchAll($sql, array($item['id']));
                $likes = count($likes_array);
                $comments_real = array();
                if ($comments){
                    foreach ($comments as $comment){
                        $sql = "SELECT * FROM user WHERE id = ?";
                        $user = $app['db']->fetchAssoc($sql, array($comment['user_id']));
                        $comment['commenter'] = $user['username'];
                        array_push($comments_real, $comment);
                    }
                }
                $item['already_liked'] = $al;
                $item['already_commented'] = $ac;
                $item['likes'] = $likes;
                $item['comments'] = $comments_real;

                array_push($last_uploaded_real, $item);
            }




        }else{
            $online_nav = false;
            $session_username = "";

            //Carrega de dades amb només imatges públiques
            $sql = "SELECT * FROM post WHERE private = 0 ORDER BY visits DESC LIMIT 5";
            $top_visited = $app['db']->fetchAll($sql);
            $top_visited_real = array();
            foreach($top_visited as $item){
                //recoger información de comentarios
                $sql = "SELECT * FROM user WHERE id = ?";
                $user = $app['db']->fetchAssoc($sql, array($item['user_id']));
                $item['username'] = $user['username'];
                $sql = "SELECT * FROM comment WHERE post_id = ?";
                $comments = $app['db']->fetchAll($sql, array($item['id']));
                $sql = "SELECT * FROM liked_image WHERE post_id = ?";
                $likes_array = $app['db']->fetchAll($sql, array($item['id']));
                $likes = count($likes_array);
                $comments_real = array();
                if ($comments){
                    foreach ($comments as $comment){
                        $sql = "SELECT * FROM user WHERE id = ?";
                        $user = $app['db']->fetchAssoc($sql, array($comment['user_id']));
                        $comment['commenter'] = $user['username'];
                        array_push($comments_real, $comment);
                    }
                }
                $item['likes'] = $likes;
                $item['comments'] = $comments_real;

                array_push($top_visited_real, $item);
            }
            $sql = "SELECT * FROM post WHERE private = 0 ORDER BY created_at DESC LIMIT 5";
            $last_uploaded = $app['db']->fetchAll($sql);
            $last_uploaded_real = array();
            foreach($last_uploaded as $item){
                //recoger información de comentarios
                $sql = "SELECT * FROM user WHERE id = ?";
                $user = $app['db']->fetchAssoc($sql, array($item['user_id']));
                $item['username'] = $user['username'];
                $sql = "SELECT * FROM comment WHERE post_id = ?";
                $comments = $app['db']->fetchAll($sql, array($item['id']));
                $sql = "SELECT * FROM liked_image WHERE post_id = ?";
                $likes_array = $app['db']->fetchAll($sql, array($item['id']));
                $likes = count($likes_array);
                $comments_real = array();
                if ($comments){
                    foreach ($comments as $comment){
                        $sql = "SELECT * FROM user WHERE id = ?";
                        $user = $app['db']->fetchAssoc($sql, array($comment['user_id']));
                        $comment['commenter'] = $user['username'];
                        array_push($comments_real, $comment);
                    }
                }
                $item['likes'] = $likes;
                $item['comments'] = $comments_real;

                array_push($last_uploaded_real, $item);
            }
        }
        //printa el twig de la home
        $content = $app['twig']->render('home.twig', array(
            'online_nav' => $online_nav,
            'session_username' => $session_username,
            'popularImages' => $top_visited_real,
            'recentImages' => $last_uploaded_real
        ));



        $response->setContent($content);
        return $response;
    }
    public function likedPost(Application $app, $id){
        $sql = "SELECT * FROM post WHERE id = ?";
        $post = $app['db']->fetchAssoc($sql, array($id));
        $notification_message = "A ".$app['session']->get('user')['username']. " li ha agradat la teva imatge \"". $post['title']."\"";
        $date = date('Y-m-d H:i:s');
        $app['db']->insert('notification', [
            'user_id' => $post['user_id'],
            'message' => $notification_message,
            'created_at' => $date
        ]);
        $app['db']->insert('liked_image', [
            'user_id' => $app['session']->get('user')['id'],
            'post_id' => $id
        ]);
        return $app->redirect('/');
    }
    public function commentedPost(Application $app, Request $request){
        $comment = $request->get('new_comment');
        $id = $request->get('post_id');
        var_dump($comment);
        $sql = "SELECT * FROM post WHERE id = ?";
        $post = $app['db']->fetchAssoc($sql, array($id));
        $notification_message = $app['session']->get('user')['username']. " ha comentat la teva imatge \"". $post['title']."\"";
        $date = date('Y-m-d H:i:s');
        var_dump($post);
        $app['db']->insert('notification', [
            'user_id' => $post['user_id'],
            'message' => $notification_message,
            'created_at' => $date
        ]);
        $app['db']->insert('comment', [
            'user_id' => $app['session']->get('user')['id'],
            'post_id' => $id,
            'content' => $comment
        ]);

        return $app->redirect('/');
    }
}