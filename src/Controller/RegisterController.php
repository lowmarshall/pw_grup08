<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 28/06/2017
 * Time: 11:36
 */

namespace SilexApp\Controller;


use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

class RegisterController
{
    public function getForm(Application $app, Request $request){
        $nom = $request->get('name');
        $email = $request->get('email');
        $naixement = $request->get('dataNaixament');
        $password = $request->get('password');
        $verificationPassword = $request->get('password-repeated');

        $profile_pic = $request->files->get('file');


        $response = new Response();

        //validacions php
        if($nom!="" && $email!="" && $naixement!="" && $password!="" && $verificationPassword!=""){
                    if (strlen($nom)>0 && strlen($nom)<=20){
                        $sql = "SELECT * FROM user WHERE username = ?";
                        $user = $app['db']->fetchAssoc($sql, array($nom));

                        if($user){
                            $response->setStatusCode(Response::HTTP_NOT_FOUND);
                            $content = $app['twig']->render('registeruser.twig', array(
                                'online_nav' => false,
                                'username' => "",
                                'email' => $email,
                                'birthdate' => $naixement,
                                'message' => "<h2>Aquest nom ja està en us</h2>"
                            ));
                        }else {
                            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                                $sql = "SELECT * FROM user WHERE email = ?";
                                $user = $app['db']->fetchAssoc($sql, array($email));
                                $response = new Response();
                                if ($user) {
                                    $content = $app['twig']->render('registeruser.twig', array(
                                        'online_nav' => false,
                                        'username' => $nom,
                                        'email' => "",
                                        'birthdate' => $naixement,
                                        'message' => "Aquest correu ja està en us"
                                    ));
                                } else {
                                    $today = date('Y-m-d');
                                    $today_time = strtotime($today);
                                    $naixement_time = strtotime($naixement);

                                    if ($naixement_time < $today_time) {
                                        $containsCap = preg_match('/[A-Z]/', $password);
                                        $containsNonCap = preg_match('/[a-z]/', $password);
                                        $containsDigit = preg_match('/\d/', $password);
                                        $strongPass = $containsCap && $containsNonCap && $containsDigit;
                                        if (strlen($password) >= 6 && strlen($password) <= 12) {
                                            if ($strongPass) {
                                                if ($password == $verificationPassword) {
                                                    if (!is_null($profile_pic)) {
                                                        $extension = $profile_pic->getClientOriginalExtension();

                                                        $imgName = uniqid() . 'profile' . $nom . '.' . $profile_pic->getClientOriginalExtension();

                                                    } else {
                                                        $extension = "png";
                                                        $imgName = 'noPic.png';
                                                    }

                                                    if (in_array($extension, array(
                                                        'gif',
                                                        'jpg',
                                                        'jpeg',
                                                        'bmp',
                                                        'png',
                                                        'GIF',
                                                        'JPG',
                                                        'JPEG',
                                                        'BMP',
                                                        'PNG'
                                                    ))) {
                                                        $app['db']->insert('user', [
                                                            'username' => $nom,
                                                            'email' => $email,
                                                            'birthdate' => $naixement,
                                                            'password' => md5($password, true),
                                                            'img_path' => $imgName
                                                        ]);
                                                        if ($imgName != 'noPic.png') {
                                                            $profile_pic->move("assets/img", $imgName);
                                                        }
                                                        $content = $app['twig']->render('registeruser.twig', array(
                                                            'online_nav' => false,
                                                            'username' => "",
                                                            'email' => "",
                                                            'birthdate' => "",
                                                            'message' => "Registre completat. Activa el teu compte aquí: <a href = '/user/activate/" . $nom . "'>Activar compte</a>"
                                                        ));
                                                    } else {
                                                        $content = $app['twig']->render('registeruser.twig', array(
                                                            'online_nav' => false,
                                                            'username' => $nom,
                                                            'email' => $email,
                                                            'birthdate' => $naixement,
                                                            'message' => "L'arxiu ha de ser PNG, JPEG, BMP o GIF"
                                                        ));
                                                    }

                                                } else {
                                                    $content = $app['twig']->render('registeruser.twig', array(
                                                        'online_nav' => false,
                                                        'username' => $nom,
                                                        'email' => $email,
                                                        'birthdate' => $naixement,
                                                        'message' => "Les contrasenyes introduides no coincideixen"
                                                    ));
                                                }
                                            } else {
                                                $content = $app['twig']->render('registeruser.twig', array(
                                                    'online_nav' => false,
                                                    'username' => $nom,
                                                    'email' => $email,
                                                    'birthdate' => $naixement,
                                                    'message' => "La contrasenya ha de portar al menys un numero y una lletra minúscula i una majúscula"
                                                ));
                                            }
                                        } else {
                                            $content = $app['twig']->render('registeruser.twig', array(
                                                'online_nav' => false,
                                                'username' => $nom,
                                                'email' => $email,
                                                'birthdate' => $naixement,
                                                'message' => "La contrasenya ha de tenir entre 6 i 12 caràcters"
                                            ));
                                        }
                                    } else {
                                        $content = $app['twig']->render('registeruser.twig', array(
                                            'online_nav' => false,
                                            'username' => $nom,
                                            'email' => $email,
                                            'birthdate' => "",
                                            'message' => "La data de naixament no pot ser futura."
                                        ));
                                    }
                                }
                            } else {
                                $content = $app['twig']->render('registeruser.twig', array(
                                    'online_nav' => false,
                                    'username' => $nom,
                                    'email' => "",
                                    'birthdate' => $naixement,
                                    'message' => "El correu ha de ser vàlid"
                                ));
                            }
                        }
                    }else{
                        $content = $app['twig']->render('registeruser.twig', array(
                            'online_nav' => false,
                            'username' => "",
                            'email' => $email,
                            'birthdate' => $naixement,
                            'message' => "El nom no pot ser major a 20 caràcters"
                        ));
                    }


        }else{
            $content = $app['twig']->render('registeruser.twig', array(
                'online_nav' => false,
                'username' => $nom,
                'email' => $email,
                'birthdate' => $naixement,
                'message' => "Omple tots els camps"
            ));
        }

        $response->setContent($content);
        return $response;

    }

    public function showForm(Application $app){
        $response = new Response();

        if ($app['session']->has('user')){
            $content = $app['twig']->render('error.twig', array(
                'online_nav' => true,
                'session_username' => $app['session']->get('user')['username'],
                'message' => "403: Accès denegat"
            ));
        }else{
            $content = $app['twig']->render('registeruser.twig', array(
                'online_nav' => false,
                'username' => "",
                'email' => "",
                'birthdate' => "",
                'message' => ""
            ));
        }


        $response->setContent($content);
        return $response;
    }
}