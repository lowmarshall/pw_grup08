<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 03/04/2017
 * Time: 16:32
 */
namespace SilexApp\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ImageController
{

    public function loadImage(Application $app, $id){
        $response = new Response();
        if ($app['session']->has('user')){
            $online_nav = true;
        }else{
            $online_nav = false;
        }
        $sql = "SELECT * FROM liked_image WHERE user_id = ? AND post_id = ?";
        $post_liked = $app['db']->fetchAssoc($sql, array($app['session']->get('user')['id'],$id));
        if($post_liked){
           $not_liked = false;
        }else{
            $not_liked = true;
        }
        $sql = "SELECT * FROM comment WHERE user_id = ? AND post_id = ?";
        $post_commented = $app['db']->fetchAssoc($sql, array($app['session']->get('user')['id'],$id));
        if($post_commented){
            $not_commented = false;
        }else{
            $not_commented = true;
        }
        $sql = "SELECT * FROM post WHERE id = ?";
        $post = $app['db']->fetchAssoc($sql, array($id));




        if ($post){

            if ($post['private']==0 || $post['user_id'] == $app['session']->get('user')['id']){
                $sql = "SELECT * FROM liked_image WHERE post_id = ?";
                $likes_array = $app['db']->fetchAll($sql, array($id));

                if($likes_array){
                    $likes = count($likes_array);
                }else{
                    $likes = 0;
                }

                $now =time();
                $post_date = strtotime($post['created_at']);
                $datediff = $now - $post_date;
                $datediff = floor($datediff /24/60/60);
                $sql = "SELECT * FROM comment WHERE post_id = ?";
                $comments = $app['db']->fetchAll($sql, array($id));
                $edited_comments = array();
                if ($comments){
                    foreach ($comments as $comment){
                        $sql = "SELECT * FROM user WHERE id = ?";
                        $user = $app['db']->fetchAssoc($sql, array($comment['user_id']));
                        $comment['commenter'] = $user['username'];
                        array_push($edited_comments, $comment);
                    }
                }
                $sql = "SELECT * FROM user WHERE id = ?";
                $poster = $app['db']->fetchAssoc($sql, array($post['user_id']));
                $visits = $post['visits'] + 1;
                $content = $app['twig']->render('image.twig', array(
                    'online_nav' => $online_nav,
                    'session_username' => $app['session']->get('user')['username'],
                    'id' => $id,
                    'not_liked' => $not_liked,
                    'not_commented' => $not_commented,
                    'title' => $post['title'],
                    'poster' => $poster['username'],
                    'visits' => $visits,
                    'likes' => $likes,
                    'comments' => $edited_comments,
                    'img_path' => $post['img_path'],
                    'days_passed' => $datediff
                ));

                $sql = "UPDATE post SET visits = ? WHERE id = ?";
                $app['db']->executeUpdate($sql, array($visits, $id));

                $response->setContent($content);
                return $response;
            }else{
                $content = $app['twig']->render('error.twig', array(
                    'online_nav' => $online_nav,
                    'session_username' => $app['session']->get('user')['username'],
                    'message' => "403: Imatge privada"
                ));
                $response->setContent($content);
                return $response;
            }

        }else{
            $content = $app['twig']->render('error.twig', array(
                'online_nav' => $online_nav,
                'session_username' => $app['session']->get('user')['username'],
                'message' => "404: Imatge no trobada"
            ));
            $response->setContent($content);
            return $response;
        }
    }




    public function likeImage(Application $app, $id){



        $sql = "SELECT * FROM post WHERE id = ?";
        $post = $app['db']->fetchAssoc($sql, array($id));
        if ($post['visits']<=1){
            $visits = $post['visits'];
        }else{
            $visits = $post['visits'] - 1;
        }
        $notification_message = "A ".$app['session']->get('user')['username']. " li ha agradat la teva imatge \"". $post['title']."\"";
        $sql = "UPDATE post SET visits = ? WHERE id = ?";
        $app['db']->executeUpdate($sql, array($visits, $id));
        $date = date('Y-m-d H:i:s');
        $app['db']->insert('notification', [
            'user_id' => $post['user_id'],
            'message' => $notification_message,
            'created_at' => $date
        ]);
        $app['db']->insert('liked_image', [
            'user_id' => $app['session']->get('user')['id'],
            'post_id' => $id
        ]);
        return $app->redirect('/image/'.$id);
    }





    public function commentImage(Application $app, Request $request, $id){
        $comment = $request->get('new_comment');
        var_dump($comment);
        $sql = "SELECT * FROM post WHERE id = ?";
        $post = $app['db']->fetchAssoc($sql, array($id));
        if ($post['visits']<=1){
            $visits = $post['visits'];
        }else{
            $visits = $post['visits'] - 1;
        }

        $notification_message = $app['session']->get('user')['username']. " ha comentat la teva imatge \"". $post['title']."\"";
        $date = date('Y-m-d H:i:s');
        $app['db']->insert('notification', [
            'user_id' => $post['user_id'],
            'message' => $notification_message,
            'created_at' => $date
        ]);
        $sql = "UPDATE post SET visits = ? WHERE id = ?";
        $app['db']->executeUpdate($sql, array($visits, $id));
        $app['db']->insert('comment', [
            'user_id' => $app['session']->get('user')['id'],
            'post_id' => $id,
            'content' => $comment
        ]);

        return $app->redirect('/image/'.$id);
    }


}