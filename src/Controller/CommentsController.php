<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 31/08/2017
 * Time: 19:33
 */

namespace SilexApp\Controller;


use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

class CommentsController
{
    public function showList(Application $app){


        $response = new Response();

        if ($app['session']->has('user')){

            $sql = "SELECT * FROM comment WHERE user_id = ?";
            $comments = $app['db']->fetchAll($sql, array($app['session']->get('user')['id']));
            $comments_real = array();
            foreach ($comments as $item){
                $sql = "SELECT * FROM post WHERE id = ?";
                $post = $app['db']->fetchAssoc($sql, array($item['post_id']));
                $item['image_id'] = $post['id'];
                $item['image_title'] = $post['title'];
                array_push($comments_real, $item);
            }

            $content = $app['twig']->render('commentlist.twig', array(
                'online_nav' => true,
                'session_username' => $app['session']->get('user')['username'],
                'comments' => $comments_real
            ));
            $response->setContent($content);
            return $response;

        }else{
            $content = $app['twig']->render('error.twig', array(
                'online_nav' => false,
                'message' => "403: Access denegat"
            ));
            $response->setContent($content);
            return $response;
        }
    }

    public function deleteComment(Application $app, $id){
        $response = new Response();
        if ($app['session']->has('user')){
            $sql = "SELECT * FROM comment WHERE id = ?";
            $comment = $app['db']->fetchAssoc($sql, array($id));
            if($comment){
                if($app['session']->get('user')['id']== $comment['user_id']){
                    $app['db']->delete('comment', array(
                        'id' => $id
                    ));
                    return $app->redirect('/user/comments');
                }else{
                    $content = $app['twig']->render('error.twig', array(
                        'online_nav' => true,
                        'session_username' => $app['session']->get('user')['username'],
                        'message' => "403: Acces denegat"
                    ));

                    $response->setContent($content);
                    return $response;
                }
            }else{
                $content = $app['twig']->render('error.twig', array(
                    'online_nav' => true,
                    'session_username' => $app['session']->get('user')['username'],
                    'message' => "404: El comentari no existeix"
                ));

                $response->setContent($content);
                return $response;
            }
        }else{
            $content = $app['twig']->render('error.twig', array(
                'online_nav' => false,
                'message' => "403: Acces denegat"
            ));

            $response->setContent($content);
            return $response;
        }


    }
}