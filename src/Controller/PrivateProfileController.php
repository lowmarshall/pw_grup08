<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 29/08/2017
 * Time: 0:43
 */

namespace SilexApp\Controller;


use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

class PrivateProfileController
{
    public function showForm(Application $app, $nom){
        $response = new Response();

        if ($app['session']->has('user')){
            $online_nav = true;
            $session_username = $app['session']->get('user')['username'];
        }else{
            $online_nav = false;
            $session_username = "";
        }
        if ($app['session']->get('user')['username']!=$nom){
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            $content = $app['twig']->render('error.twig', array(
                'online_nav' => $online_nav,
                'session_username' => $session_username,
                'message' => "403: Accès denegat"
            ));
        }else{

            $sql = "SELECT * FROM user WHERE username = ?";
            $user = $app['db']->fetchAssoc($sql, array($nom));
            if($user){
                $response->setStatusCode(Response::HTTP_OK);
                $content = $app['twig']->render('privateprofile.twig', array(

                    'online_nav' => $online_nav,
                    'session_username' => $session_username,
                    'real_username' => $nom,
                    'real_email' => $user['email'],
                    'real_birthday' => $user['birthdate'],
                    'real_profilepic' => $user['img_path'],
                    'new_username' => "",
                    'new_email' => "",
                    'new_birthday' => "",
                    'message' => ""
                ));
            }else{
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
                $content = $app['twig']->render('error.twig', array(

                    'online_nav' => $online_nav,
                    'session_username' => $session_username,
                    'message' => "403: Accès denegat"
                ));
            }

        }


        $response->setContent($content);
        return $response;
    }

    public function getForm(Application $app, Request $request){

        $nom = $request->get('name');
        $email = $request->get('email');
        $birthday = $request->get('birthday');
        $old_pass = $request->get('old_pass');
        $new_pass = $request->get('new_pass');
        $new_pass_again = $request->get('new_pass-again');
        $new_profile_pic = $request->files->get('file');
        $deletepic = $request->get('delete');

        $response = new Response();

        $sql = "SELECT * FROM user WHERE username = ?";
        $user = $app['db']->fetchAssoc($sql, array($app['session']->get('user')['username']));

        $updated_name = $user['username'];
        $updated_email = $user['email'];
        $updated_birthday = $user['birthdate'];
        $updated_pass = $user['password'];
        $updated_pic= $user['img_path'];

        $error = array(
            'errors' => array(
                'error_name' => false,
                'error_email' => false,
                'error_birthday' => false,
                'error_pass' => false,
                'error_img' => false
            ),
            'messages' => array(
                'message_name' => "",
                'message_email' => "",
                'message_birthday' => "",
                'message_pass' => "",
                'message_img' => ""
            )
        );

        if($nom != ""){
            if (strlen($nom)>0 && strlen($nom)<=20){
                $sql = "SELECT * FROM user WHERE username = ?";
                $previous_user = $app['db']->fetchAssoc($sql, array($nom));
                if ($previous_user){
                    $error['errors']['error_name'] = true;
                    $error['messages']['message_name'] = "Aquest nom ja està en us";
                }else{
                    $updated_name = $nom;
                }
            }else{
                $error['errors']['error_name'] = true;
                $error['messages']['message_name'] = "El nom no pot ser major a 20 caràcters";
            }
        }
        if ($email != ""){
            if (filter_var($email,FILTER_VALIDATE_EMAIL)){
                $sql = "SELECT * FROM user WHERE email = ?";
                $previous_user = $app['db']->fetchAssoc($sql, array($email));
                if ($previous_user){
                    $error['errors']['error_email'] = true;
                    $error['messages']['message_email'] = "Aquest email ja està en us";
                }else{
                    $updated_email = $email;
                }

            }else{
                $error['errors']['error_email'] = true;
                $error['messages']['message_email'] = "El correu ha de ser vàlid";
            }
        }
        if ($birthday != ""){
            $today = new DateTime();
            if ($birthday<$today){
                $updated_birthday = $birthday;
            }else{
                $error['errors']['error_birthday'] = true;
                $error['messages']['message_birthday'] = "La data de naixament no pot ser futura.";
            }
        }
        if ($old_pass != "" || $new_pass != "" || $new_pass_again != ""){
            if ($old_pass != "" && $new_pass != "" && $new_pass_again != ""){
                if (md5($old_pass,true)==$updated_pass){
                    $containsCap = preg_match('/[A-Z]/',$new_pass);
                    $containsNonCap = preg_match('/[a-z]/',$new_pass);
                    $containsDigit = preg_match('/\d/',$new_pass);
                    $strongPass = $containsCap && $containsNonCap && $containsDigit;
                    if(strlen($new_pass)>=6 && strlen($new_pass)<=12) {
                        if ($strongPass) {
                            if ($new_pass == $new_pass_again){
                                $updated_pass = md5($new_pass,true);
                            }else{
                                $error['errors']['error_pass'] = true;
                                $error['messages']['message_pass'] = "Les contrasenyes introduides no coincideixen";
                            }
                        }else{
                            $error['errors']['error_pass'] = true;
                            $error['messages']['message_pass'] = "La contrasenya ha de portar al menys un numero y una lletra minúscula i una majúscula";
                        }
                    }else{
                        $error['errors']['error_pass'] = true;
                        $error['messages']['message_pass'] = "La contrasenya ha de tenir entre 6 i 12 caràcters";
                    }
                }else{
                    $error['errors']['error_pass'] = true;
                    $error['messages']['message_pass'] = "La contrasenya antiga no coincideix";
                }
            }else{
                $error['errors']['error_pass'] = true;
                $error['messages']['message_pass'] = "Omple tots els camps de contrasenyes";
            }
        }
        if (!is_null($new_profile_pic)){
            $extension = $new_profile_pic->getClientOriginalExtension();
            if (in_array($extension, array('gif', 'jpg', 'jpeg', 'bmp', 'png', 'GIF', 'JPG', 'JPEG', 'BMP', 'PNG'))){
                $updated_pic = uniqid().'.'.$extension;
                $new_profile_pic->move("assets/img", $updated_pic);
            }else{
                $error['errors']['error_pic'] = true;
                $error['messages']['message_pic'] = "El format de la imatge ha de ser PNG, JPEG, GIF o BMP";
            }


        }else{
            if ($deletepic){
                $updated_pic = "noPic.png";
            }
        }
        if (!in_array('true', $error['errors'], true)){
            $sql = "UPDATE user SET username = ?, email = ?, birthdate = ?, password = ?, img_path = ? WHERE id = ?";
            $app['db']->executeUpdate($sql, array($updated_name, $updated_email, $updated_birthday, $updated_pass, $updated_pic, $user['id']));
            $app['session']->set('user', array('username' => $updated_name, 'email' => $updated_email, 'profilePic' => $updated_pic, 'id' => $user['id']));
            $content = $app['twig']->render('privateprofile.twig', array(

                'online_nav' => true,
                'session_username' => $app['session']->get('user')['username'],
                'real_username' => $updated_name,
                'real_email' => $updated_email,
                'real_birthday' => $updated_birthday,
                'real_profilepic' => $updated_pic,
                'new_username' => "",
                'new_email' => "",
                'new_birthday' => "",
                'message' => "Actualització d'informació correcta"
            ));

        }else{
            $fullmessage = "";
            foreach ($error['errors'] as $valor){
                if (!$valor){
                    $key = key($error['errors']);
                    $newmessage = array_search($key, array_keys($error['messages']));
                    $fullmessage .= $newmessage."<br>";
                }
            }
            $content = $app['twig']->render('privateprofile.twig', array(
                'online_nav' => true,
                'session_username' => $app['session']->get('user')['username'],
                'real_username' => $user['username'],
                'real_email' => $user['email'],
                'real_birthday' => $user['birthdate'],
                'real_profilepic' => $user['img_path'],
                'new_username' => $nom,
                'new_email' => $email,
                'new_birthday' => $birthday,
                'message' => $fullmessage
            ));
        }


        $response->setContent($content);
        return $response;
    }
}