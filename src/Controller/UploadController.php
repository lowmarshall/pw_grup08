<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 30/08/2017
 * Time: 16:13
 */

namespace SilexApp\Controller;


use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UploadController
{
    public function uploadImage(Application $app, Request $request)
    {
        $imatge = $request->files->get('file');
        $titol = $request->get('title');
        $privada = $request->get('private');

        $response = new Response();



        $error = array(
            'errors' => array(
                'error_img' => false,
                'error_title' => false,
                'error_global' => false
            ),
            'messages' => array(
                'message_img' => "",
                'message_title' => "",
                'message_global' => ""
            )
        );

        if (is_null($imatge) || $titol ==""){
            $error['errors']['error_global'] = true;
            $error['messages']['message_global'] = "Omple tots els camps";
        }
        if(!is_null($imatge)){
            $extension = $imatge->getClientOriginalExtension();
            if (!in_array($extension, array('gif', 'jpg', 'jpeg', 'bmp', 'png', 'GIF', 'JPG', 'JPEG', 'BMP', 'PNG'))){
                $error['errors']['error_pic'] = true;
                $error['messages']['message_pic'] = "El format de la imatge ha de ser PNG, JPEG, GIF o BMP";
            }
        }
        if($titol != ""){
            if ($titol>200){
                $error['errors']['error_title'] = true;
                $error['messages']['message_title'] = "El títol no pot ser més llarg de 200 caràcters.";
            }
        }


        if($privada){
            $private = 1;
        }else{
            $private = 0;
        }

        if (!in_array('true', $error['errors'], true)){
            $nom_imatge = uniqid().'.'.$extension;
            $imatge->move("assets/img", $nom_imatge);
            $date = date('Y-m-d H:i:s');
            $app['db']->insert('post', [
                'user_id' => $app['session']->get('user')['id'],
                'title' => $titol,
                'img_path' => $nom_imatge,
                'visits' => 0,
                'private' => $private,
                'created_at' => $date
            ]);


            return $app->redirect('/');

        }else{
            $fullmessage = "";
            foreach ($error['errors'] as $valor){
                if (!$valor){
                    $key = key($error['errors']);
                    $newmessage = array_search($key, array_keys($error['messages']));
                    $fullmessage .= $newmessage."<br>";
                }
            }
            $content = $app['twig']->render('upload.twig', array(
                'online_nav' => true,
                'session_username' => $app['session']->get('user')['username'],
                'title' => "",
                'message' => $fullmessage
            ));
        }
    }
    public function showForm(Application $app){
        $response = new Response();

        if ($app['session']->has('user')){

            $content = $app['twig']->render('upload.twig', array(
                'online_nav' => true,
                'session_username' => $app['session']->get('user')['username'],
                'title' => "",
                'message' => ""
            ));
        }else{
            $content = $app['twig']->render('error.twig', array(
                'online_nav' => false,
                'message' => "403: Accès denegat"
            ));
        }
        $response->setContent($content);
        return $response;
    }
}