<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 30/08/2017
 * Time: 14:58
 */

namespace SilexApp\Controller;


use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

class NotificationController
{
    public function showNotifications(Application $app){
        $response = new Response();

        if ($app['session']->has('user')){

            $sql = "SELECT * FROM notification WHERE user_id = ? ORDER BY created_at DESC";
            $notifications = $app['db']->fetchAll($sql, array($app['session']->get('user')['id']));

            $content = $app['twig']->render('notifications.twig', array(
                'online_nav' => 1,
                'session_username' => $app['session']->get('user')['username'],
                'notifications' => $notifications
            ));
        }else{
            $content = $app['twig']->render('error.twig', array(
                'online_nav' => 0,
                'message' => "403: accès denegat"
            ));
        }

        $response->setContent($content);
        return $response;
    }
}