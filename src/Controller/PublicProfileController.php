<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 29/08/2017
 * Time: 0:45
 */

namespace SilexApp\Controller;


use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

class PublicProfileController
{
    public function showPage(Application $app, $nom){
        $response = new Response();

        if ($app['session']->has('user')){
            $online_nav = true;
            $session_username = $app['session']->get('user')['username'];
        }else{
            $online_nav = false;
            $session_username = "";
        }



        $sql = "SELECT * FROM user WHERE username = ?";
        $user = $app['db']->fetchAssoc($sql, array($nom));
        if($user) {
            $response->setStatusCode(Response::HTTP_OK);
            $sql = "SELECT * FROM post WHERE user_id = ? ORDER BY created_at DESC";
            $list = $app['db']->fetchAll($sql, array($user['id']));
            if ($list){
                $post_num = count($list);
            }else{
                $post_num = 0;
            }

            $sql = "SELECT * FROM comment WHERE user_id = ?";
            $comments = $app['db']->fetchAll($sql, array($user['id']));
            if ($comments){
                $comment_num = count($comments);
            }else{
                $comment_num = 0;
            }


            $content = $app['twig']->render('publicprofile.twig', array(
                'online_nav' => $online_nav,
                'session_username' => $session_username,
                'username' => $user['username'],
                'profile_image' => $user['img_path'],
                'post_num' => $post_num,
                'comment_num' => $comment_num,
                'list' => $list
            ));
        }else{
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            $content = $app['twig']->render('error.twig', array(
                'online_nav' => $online_nav,
                'message' => "404: No s'ha trobat l'usuari a la base de dades"
            ));
        }
        $response->setContent($content);
        return $response;
    }
}