<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 24/08/2017
 * Time: 0:40
 */

namespace SilexApp\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class LoginController
{

    public function showForm(Application $app){
        $response = new Response();
        session_start();
        if ($app['session']->has('user')){
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            $content = $app['twig']->render('error.twig', array(
                'online_nav' => true,
                'session_username' => $app['session']->get('user')['username'],
                'message' => "403: Accès denegat. Ja et trobes connectat."
            ));
        }else {
            $response->setStatusCode(Response::HTTP_OK);
            $content = $app['twig']->render('loginuser.twig',[
                'online_nav' => false,
                'login' => "",
                'message' => ""
            ]);

        }
        $response->setContent($content);
        return $response;
    }

    public function getForm(Application $app, Request $request){
        $login = $request->get('login');
        $password = $request->get('password');
        $response = new Response();
        //validacions php
        $sql = "SELECT * FROM user WHERE username = ? OR email = ?";
        $user = $app['db']->fetchAssoc($sql, array($login, $login));

        if($user){
            if($user['active']==1){
                if($user['password']==md5($password, true)){
                    $app['session']->set('user', array('username' => $user['username'], 'email' => $user['email'], 'id' => $user['id'], 'profilePic' => $user['img_path']));

                    return $app->redirect('/');

                }else{
                    $content = $app['twig']->render('loginuser.twig',[
                        'online_nav' => false,
                        'login' => $login,
                        'message' => "La contrasenya no es correcta"
                    ]);
                    $response->setContent($content);
                    return $response;
                }
            }else{
                $content = $app['twig']->render('loginuser.twig',[
                    'online_nav' => false,
                    'login' => $login,
                    'message' => "La compta no està activada"
                ]);
                $response->setContent($content);
                return $response;
            }

        }else{
            $content = $app['twig']->render('loginuser.twig',[
                'online_nav' => false,
                'login' => $login,
                'message' => "Aquest usuari no esta a la base de dades"
            ]);
            $response->setContent($content);
            return $response;
        }
    }



}