<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 30/08/2017
 * Time: 16:13
 */

namespace SilexApp\Controller;


use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ImageEditController
{
    public function getForm(Application $app, Request $request, $id)
    {

        $titol = $request->get('title');
        $privada = $request->get('private');

        $response = new Response();

        $sql = "SELECT * FROM post WHERE id = ?";
        $post = $app['db']->fetchAssoc($sql, array($id));


        $updated_title = $post['title'];

        $error = false;
        $errorMessage = "";

        if($titol != ""){
            if ($titol>200){
                $error = true;
                $errorMessage = "El títol no pot ser més llarg de 200 caràcters.";
            }else{
                $updated_title = $titol;
            }
        }


        if($privada){
            $private = 1;
        }else{
            $private = 0;
        }

        if (!$error){

            $sql = "UPDATE post SET title = ?, private = ? WHERE id = ?";
            $app['db']->executeUpdate($sql, array($updated_title, $private, $id));

            return $app->redirect('/image/'.$id);

        }else{

            $content = $app['twig']->render('editimage.twig', array(
                'online_nav' => true,
                'session_username' => $app['session']->get('user')['username'],
                'img_path' => $post['img_path'],
                'old_title' => $post['title'],
                'title' => $titol,
                'message' => $errorMessage
            ));
            $response->setContent($content);
            return $response;
        }
    }
    public function showForm(Application $app, $id){
        $response = new Response();

        if ($app['session']->has('user')){

            $sql = "SELECT * FROM post WHERE id = ?";
            $post = $app['db']->fetchAssoc($sql, array($id));

            $content = $app['twig']->render('editimage.twig', array(
                'online_nav' => true,
                'session_username' => $app['session']->get('user')['username'],
                'img_path' => $post['img_path'],
                'old_title' => $post['title'],
                'title' => "",
                'private' => $post['private'],
                'message' => ""
            ));
        }else{
            $content = $app['twig']->render('error.twig', array(
                'online_nav' => false,
                'message' => "403: Accès denegat"
            ));
        }
        $response->setContent($content);
        return $response;
    }
}