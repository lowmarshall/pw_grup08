<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 29/08/2017
 * Time: 2:41
 */

namespace SilexApp\Controller;


use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

class ActivationController
{
    public function activateAccount(Application $app, $nom){
        $response = new Response();
        if ($app['session']->has('user')){
            $content = $app['twig']->render('error.twig', array(
                'online_nav' => false,
                'message' => "403: Accès denegat"
            ));
            $response->setContent($content);
            return $response;
        }else{
            $sql = "UPDATE user SET active = 1 WHERE username = ?";
            $app['db']->executeUpdate($sql, array($nom));
            return $app->redirect('/user/login');

        }
        
    }
}